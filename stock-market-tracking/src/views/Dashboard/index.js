import React, { useEffect } from 'react';
import StockMarketBar from '../../components/StockMarketBar';
import SideBar from '../../components/Menu';

function Dashboard() {

  return (
    <div>
      <StockMarketBar />
      <SideBar />
    </div>
  );
}

export default Dashboard;
