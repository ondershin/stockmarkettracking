import React from 'react';
import { Button, Navbar,Alert, Form, FormControl, Table } from 'react-bootstrap';
import 'react-pro-sidebar/dist/css/styles.css';
import openIcon from '../../images/collapse.svg';
import logo from '../../images/logo.png';

function Dashboard(props) {
  const [open, setOpen] = React.useState(false);

  const handleOpenClick = () => {
    if (open === true) setOpen(false);
    else setOpen(true);
  };

  const SideBarTable = () => {
    if (open === true) {
      return (
        <div>
          <Alert style={{color:'#000000',fontWeight: 'bold'}} variant='light'>
            Watchlist
          </Alert>
          <Table striped bordered hover size='sm'>
            <thead>
              <tr>
                <th>Symbol</th>
                <th>Las</th>
                <th>Chg</th>
                <th>Chg%</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>AAPL</td>
                <td>438.66</td>
                <td style={{ color: '#26a69a' }}>2.91</td>
                <td style={{ color: '#26a69a' }}>0.67%</td>
              </tr>
            </tbody>
          </Table>
        </div>
      );
    }
  };

  return (
    <div>
      <Navbar style={{ backgroundColor: '#1a233a' }}>
        <Navbar.Brand href='#home'>
          <img alt='logo' src={logo} />
        </Navbar.Brand>
        <Form inline style={{ position: 'absolute', right: '5vw' }}>
          <FormControl
            type='text'
            placeholder='Search'
            style={{ backgroundColor: '#1a233a', color: 'white' }}
            className='mr-sm-2'
          />
        </Form>
      </Navbar>

      <div
        style={{
          backgroundColor: '#fff',
          width: open ? '50vw' : '45px',
          height: ' 82.5vh',
        }}
      >
        <Button
          variant='dark'
          style={{
            borderRadius: 0,
            width: '45px',
            height: '45px',
            padding: '10px',
          }}
          onClick={handleOpenClick}
        >
          <img alt='openIcon' className='img-fluid svg-icon' src={openIcon} />
        </Button>
        {SideBarTable()}
      </div>
    </div>
  );
}

export default Dashboard;
